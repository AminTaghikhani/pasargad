import * as moment from 'moment';
import { Injectable } from '@nestjs/common';
import { OverlapDTO } from './models/dto';
import Rectangle from './models/rectangle';
import { InjectRepository } from '@nestjs/typeorm';
import { RectangleEntity } from './models/rectangle.entity';
import { Repository } from 'typeorm';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(RectangleEntity)
    private rectangleRepo: Repository<RectangleEntity>,
  ) {}
  async saveOverlapped(model: OverlapDTO) {
    const results = [];
    const main = new Rectangle(model.main);
    for (let i = 0; i < model.input.length; i++) {
      const target = new Rectangle(model.input[i]);
      if (main.isOverlapWith(target)) {
        results.push({
          ...model.input[i],
          time: moment().format('YYYY/MM/DD HH:mm:ss'),
        });
      }
    }
    await this.mapAndSave(results);
    return results;
  }

  async mapAndSave(results) {
    const entities = results.map((result) => {
      return this.rectangleRepo.create({
        x: result.x,
        y: result.y,
        width: result.width,
        height: result.height,
        time: result.time,
      });
    });
    await this.save(entities);
  }

  async save(entities) {
    await this.rectangleRepo.save(entities);
  }

  async getOverlaps() {
    return await this.rectangleRepo.find();
  }
}
