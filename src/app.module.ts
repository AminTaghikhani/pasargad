import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import * as ormconfig from './ormconfig';
import { RectangleEntity } from './models/rectangle.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot(ormconfig),
    TypeOrmModule.forFeature([RectangleEntity]),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
