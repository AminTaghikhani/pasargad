import { BaseEntity, Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

const TABLE_NAME = 'rectangles';

@Entity(TABLE_NAME)
export class RectangleEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  rowId: number;
  @Column()
  x: number;
  @Column()
  y: number;
  @Column()
  width: number;
  @Column()
  height: number;
  @Column({ type: 'timestamp with time zone' })
  time: Date;
}
