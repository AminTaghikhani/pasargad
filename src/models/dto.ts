export class RectangleDTO {
  x: number;
  y: number;
  width: number;
  height: number;
}

export class OverlapDTO {
  main: RectangleDTO;
  input: RectangleDTO[];
}
