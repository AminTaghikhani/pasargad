import Point from './point';
import { RectangleDTO } from './dto';

export default class Rectangle {
  private readonly x: number;
  private readonly y: number;
  private readonly width: number;
  private readonly height: number;
  private _lt: Point;
  private _rb: Point;

  constructor(dto: RectangleDTO) {
    this.x = dto.x;
    this.y = dto.y;
    this.width = dto.width;
    this.height = dto.height;
    this.calculateCorners();
  }

  // constructor(
  //   private x: number,
  //   private y: number,
  //   private width: number,
  //   private height: number,
  // ) {
  //   this.calculateCorners();
  // }

  get lt(): Point {
    return this._lt;
  }

  set lt(value: Point) {
    this._lt = value;
  }

  get rb(): Point {
    return this._rb;
  }

  set rb(value: Point) {
    this._rb = value;
  }

  calculateCorners() {
    this.lt = new Point(this.x, this.y + this.height);
    this.rb = new Point(this.x + this.width, this.y);
  }

  isOverlapWith(rect: Rectangle): boolean {
    if (
      this.lt.x === this.rb.x ||
      this.lt.y === this.rb.y ||
      rect.lt.x === rect.rb.x ||
      rect.lt.y === rect.rb.y
    ) {
      return false;
    }
    if (this.lt.x >= rect.rb.x || rect.lt.x >= this.rb.x) {
      return false;
    }
    if (this.rb.y >= rect.lt.y || rect.rb.y >= this.lt.y) {
      return false;
    }
    return true;
  }
}
