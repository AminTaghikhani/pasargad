import { MigrationInterface, QueryRunner } from 'typeorm';

export class addRectangleTable1628664816541 implements MigrationInterface {
  name = 'addRectangleTable1628664816541';

  public async up(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(
      `CREATE TABLE "rectangles" ("rowId" SERIAL NOT NULL, "x" integer NOT NULL, "y" integer NOT NULL, "width" integer NOT NULL, "height" integer NOT NULL, "time" TIMESTAMP WITH TIME ZONE NOT NULL, CONSTRAINT "PK_3627f8b9d567ba952e4e8ccfec6" PRIMARY KEY ("rowId"))`,
    );
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.query(`DROP TABLE "rectangles"`);
  }
}
