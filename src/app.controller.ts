import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { OverlapDTO } from './models/dto';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getOverlaps() {
    const items = await this.appService.getOverlaps();
    return items.map((item) => {
      return {
        x: item.x,
        y: item.y,
        width: item.width,
        height: item.height,
        time: item.time,
      };
    });
  }

  @Post()
  async postRectangle(@Body() model: OverlapDTO) {
    return await this.appService.saveOverlapped(model);
  }
}
